package cat.guarch.asistenciaaula;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import java.util.List;

import cat.guarch.asistenciaaula.model.entitiesLayer.entities.cursos.Curso;
import cat.guarch.asistenciaaula.model.serviceLayer.managers.ServiceManager;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ServiceManager serviceManager = new ServiceManager();



        List<Curso> cursos = serviceManager.getCursoService().getCursos();
        serviceManager.getCursoService().saveCurso(cursos.get(0), this);

        Curso curso = serviceManager.getCursoService().getCurso();
    }
}

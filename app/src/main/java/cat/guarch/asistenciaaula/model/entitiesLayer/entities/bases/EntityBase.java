package cat.guarch.asistenciaaula.model.entitiesLayer.entities.bases;

import java.util.UUID;

public abstract class EntityBase {
    private UUID id;


    ////// CONSTRUCTORS //////

    public EntityBase(){
        setId(UUID.randomUUID());
    }


    ////// GETTERS & SETTERS //////

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }
}

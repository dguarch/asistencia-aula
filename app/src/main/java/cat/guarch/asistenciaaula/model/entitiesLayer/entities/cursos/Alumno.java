package cat.guarch.asistenciaaula.model.entitiesLayer.entities.cursos;

import cat.guarch.asistenciaaula.model.entitiesLayer.entities.bases.EntityBase;

public class Alumno extends EntityBase {

    private String nombre;
    private String codigoAlumno;


    public Alumno() {
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCodigoAlumno() {
        return codigoAlumno;
    }

    public void setCodigoAlumno(String codigoAlumno) {
        this.codigoAlumno = codigoAlumno;
    }
}



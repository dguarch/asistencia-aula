package cat.guarch.asistenciaaula.model.entitiesLayer.entities.cursos;

import java.util.Date;

import cat.guarch.asistenciaaula.model.entitiesLayer.entities.bases.EntityBase;

public class Asistencia extends EntityBase {

    private Date inicio;
    private Date fin;
    private Alumno alumno;

    public Asistencia(){
    }


    public Date getInicio() {
        return inicio;
    }

    public void setInicio(Date inicio) {
        this.inicio = inicio;
    }

    public Date getFin() {
        return fin;
    }

    public void setFin(Date fin) {
        this.fin = fin;
    }

    public Alumno getAlumno() {
        return alumno;
    }

    public void setAlumno(Alumno alumno) {
        this.alumno = alumno;
    }
}

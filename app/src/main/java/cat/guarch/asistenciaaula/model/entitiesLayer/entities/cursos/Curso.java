package cat.guarch.asistenciaaula.model.entitiesLayer.entities.cursos;

import java.util.ArrayList;
import java.util.List;

import cat.guarch.asistenciaaula.model.entitiesLayer.entities.bases.EntityBase;

public class Curso extends EntityBase {

    private String titulo;
    private List<Sesion> sesiones;


    ////// CONSTRUCTORS //////

    public Curso(){
        setSesiones(new ArrayList<Sesion>());
    }


    ////// GETTERS & SETTERS //////

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public List<Sesion> getSesiones() {
        return sesiones;
    }

    public void setSesiones(List<Sesion> sesiones) {
        this.sesiones = sesiones;
    }
}

package cat.guarch.asistenciaaula.model.entitiesLayer.entities.cursos;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import cat.guarch.asistenciaaula.model.entitiesLayer.entities.bases.EntityBase;

public class Sesion extends EntityBase {

    private LocalDateTime inicio;
    private LocalDateTime fin;
    private List<Asistencia> asistencias;


    ////// CONSTRUCTORS //////

    public Sesion(){
        setAsistencias(new ArrayList<Asistencia>());
    }


    ////// GETTERS & SETTERS //////

    public LocalDateTime getInicio() {
        return inicio;
    }

    public void setInicio(LocalDateTime inicio) {
        this.inicio = inicio;
    }

    public LocalDateTime getFin() {
        return fin;
    }

    public void setFin(LocalDateTime fin) {
        this.fin = fin;
    }

    public List<Asistencia> getAsistencias() {
        return asistencias;
    }

    public void setAsistencias(List<Asistencia> asistencias) {
        this.asistencias = asistencias;
    }
}


package cat.guarch.asistenciaaula.model.persistenceLayer.api.cursos;

import android.content.Context;

import cat.guarch.asistenciaaula.model.entitiesLayer.entities.cursos.Curso;

public interface ICursoDAO {

    void saveCurso(Curso curso, Context context);

    Curso getCurso();
}

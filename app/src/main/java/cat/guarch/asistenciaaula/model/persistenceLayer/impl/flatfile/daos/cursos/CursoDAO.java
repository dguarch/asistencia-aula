package cat.guarch.asistenciaaula.model.persistenceLayer.impl.flatfile.daos.cursos;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;

import java.io.OutputStreamWriter;

import cat.guarch.asistenciaaula.model.entitiesLayer.entities.cursos.Curso;
import cat.guarch.asistenciaaula.model.persistenceLayer.api.cursos.ICursoDAO;

public class CursoDAO implements ICursoDAO {

    @Override
    public void saveCurso(Curso curso, Context context) {
        try
        {
            Gson gson = new Gson();
            OutputStreamWriter fout=
                    new OutputStreamWriter(
                            context.openFileOutput("Curso.json", Context.MODE_PRIVATE));

            fout.write(gson.toJson(curso));
            fout.close();
        }
        catch (Exception ex)
        {
            Log.e("Ficheros", "Error al escribir fichero a memoria interna");
        }
    }

    @Override
    public Curso getCurso() {
        Curso curso = new Curso();

        try {
            Gson gson = new Gson();
            


            Curso cursoLeido = gson.fromJson("Curso.json", Curso.class);
        }
        catch (Exception ex){
            Log.e("Ficheros", "Error al leer fichero a memoria interna");
        }
        return curso;
    }

}

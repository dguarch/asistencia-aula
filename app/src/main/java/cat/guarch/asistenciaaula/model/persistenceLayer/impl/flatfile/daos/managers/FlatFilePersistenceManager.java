package cat.guarch.asistenciaaula.model.persistenceLayer.impl.flatfile.daos.managers;

import cat.guarch.asistenciaaula.model.persistenceLayer.api.cursos.ICursoDAO;
import cat.guarch.asistenciaaula.model.persistenceLayer.impl.flatfile.daos.cursos.CursoDAO;
import cat.guarch.asistenciaaula.model.persistenceLayer.managers.PersistenceManager;

public class FlatFilePersistenceManager extends PersistenceManager {

    private ICursoDAO cursoDAO;

    @Override
    public ICursoDAO getCursoDAO() {
        if (cursoDAO == null){
            cursoDAO = new CursoDAO();
        }
        return cursoDAO;
    }
}

package cat.guarch.asistenciaaula.model.persistenceLayer.managers;

import java.util.ArrayList;
import java.util.List;

import cat.guarch.asistenciaaula.model.persistenceLayer.api.cursos.ICursoDAO;
import cat.guarch.asistenciaaula.model.persistenceLayer.impl.flatfile.daos.managers.FlatFilePersistenceManager;

public abstract class PersistenceManager {

    public abstract ICursoDAO getCursoDAO();

    public static List<PersistenceManager> getPersistenceManagers(){
        List<PersistenceManager> persistenceManagers = new ArrayList<PersistenceManager>();

        persistenceManagers.add(new FlatFilePersistenceManager());

        return persistenceManagers;
    }
}

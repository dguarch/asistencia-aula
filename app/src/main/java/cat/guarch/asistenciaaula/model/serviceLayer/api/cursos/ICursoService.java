package cat.guarch.asistenciaaula.model.serviceLayer.api.cursos;

import android.content.Context;

import java.util.List;

import cat.guarch.asistenciaaula.model.entitiesLayer.entities.cursos.Alumno;
import cat.guarch.asistenciaaula.model.entitiesLayer.entities.cursos.Curso;

public interface ICursoService {

    List<Curso> getCursos();

    Curso getCurso();

    List<Curso> getCursoByAlumno(Alumno alumno);

    List<Curso> generateCursoMock();

    void saveCurso(Curso curso, Context context);


}

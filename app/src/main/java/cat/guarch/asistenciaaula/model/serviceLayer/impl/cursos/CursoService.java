package cat.guarch.asistenciaaula.model.serviceLayer.impl.cursos;

import android.content.Context;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import cat.guarch.asistenciaaula.model.entitiesLayer.entities.cursos.Alumno;
import cat.guarch.asistenciaaula.model.entitiesLayer.entities.cursos.Curso;
import cat.guarch.asistenciaaula.model.entitiesLayer.entities.cursos.Sesion;
import cat.guarch.asistenciaaula.model.persistenceLayer.impl.flatfile.daos.managers.FlatFilePersistenceManager;
import cat.guarch.asistenciaaula.model.persistenceLayer.managers.PersistenceManager;
import cat.guarch.asistenciaaula.model.serviceLayer.api.cursos.ICursoService;

public class CursoService implements ICursoService {

    private List<PersistenceManager> persistenceManager;

    public CursoService(List<PersistenceManager> persistenceManager){
        this.persistenceManager = persistenceManager;
    }

    @Override
    public List<Curso> getCursos() {
        return generateCursoMock();
    }

    @Override
    public List<Curso> getCursoByAlumno(Alumno alumno) {
        return null;
    }

    @Override
    public List<Curso> generateCursoMock() {
        List<Curso> listaCursos= new ArrayList<Curso>();

        Curso c1 = new Curso();
        listaCursos.add(c1);
        c1.setTitulo("Curso Java");

        Sesion s1 = new Sesion();
        s1.setInicio(LocalDateTime.of(2018,10,02,19,30));
        s1.setFin(LocalDateTime.of(2018,10,04,22,00));

        Sesion s2 = new Sesion();
        s2.setInicio(LocalDateTime.of(2018,11,03,19,30));
        s2.setFin(LocalDateTime.of(2018,11,05,22,00));

        c1.getSesiones().add(s1);
        c1.getSesiones().add(s2);

        return listaCursos;
    }

    @Override
    public void saveCurso(Curso curso, Context context) {
        //this.persistenceManager.get(0).getCursoDAO().saveCurso(curso);
        PersistenceManager fpm = this.persistenceManager.stream().filter(pm -> pm instanceof FlatFilePersistenceManager).findAny().get();
        fpm.getCursoDAO().saveCurso(curso,context);
    }

    @Override
    public Curso getCurso() {
        return null;
    }


}

package cat.guarch.asistenciaaula.model.serviceLayer.managers;

import cat.guarch.asistenciaaula.model.serviceLayer.api.cursos.ICursoService;

public interface IServiceManager {

    ICursoService getCursoService();
}

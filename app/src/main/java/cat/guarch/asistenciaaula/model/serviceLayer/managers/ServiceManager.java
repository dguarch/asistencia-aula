package cat.guarch.asistenciaaula.model.serviceLayer.managers;

import java.util.List;

import cat.guarch.asistenciaaula.model.persistenceLayer.managers.PersistenceManager;
import cat.guarch.asistenciaaula.model.serviceLayer.api.cursos.ICursoService;
import cat.guarch.asistenciaaula.model.serviceLayer.impl.cursos.CursoService;

public class ServiceManager implements IServiceManager {

    private ICursoService cursoService;
    private List<PersistenceManager> persistenceManagers;

    public ServiceManager(){
        persistenceManagers = PersistenceManager.getPersistenceManagers();
    }

    @Override
    public ICursoService getCursoService() {
        if (cursoService == null){
            cursoService = new CursoService(persistenceManagers);
        }
        return cursoService;
    }
}
